/* ****************************************************************************
-- Project        : Syntell MX - Services Module
-- File Name      : main.c
-- Description    : This file is the 'c' entry point and implements the 
--                  metrology for the mains characteristics. 
-- Author(s)      : P.G. Koeppen, (www.octaplex.com)
-- History        :  1 - Initial Release - 25-September-2012
--                   2 - Major code cleanup - February-2013
--                  <Revision changed> - <change> - <date changed>
-- Notes          : This is based on sample firmware described in 
-- application note SLAA949 "Implementation of a Single-Phase Electronic 
-- Watt-Hour Meter Using the MSP430AFE2xx" and firmware found at
-- http://www-s.ti.com/sc/techlit/slaa494.zip.
-- ****************************************************************************
--
-- This file is derived from 'emeter-foreground.c' and the original license
-- is reproduced below.
--
-- Changes include the adaptations for Code Composer Studio 5 and the unique
-- messaging parameters required for the MX project.
--
-- ****************************************************************************/

//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  coypright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-foreground.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-foreground.c,v 1.34 2008/10/28 10:13:40 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
//  MSP430 foreground (non-interrupt) routines for e-meters
//
//  This software is appropriate for single phase e-meters
//  using a voltage sensor plus a CT or shunt resistor current sensors, or
//  a combination of a CT plus a shunt.
// 
//    Foreground process includes:
//    -Using timer tick to wait
//    -Calculating the power per channel
//    -Determine if current channel needs scaling.
//    -Determine if needs to be in low power modes.
//    -Compensate reference from temperature sensor
//
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <msp430afe253.h>
#include "emeter-toolkit\\emeter-toolkit.h"
#include "emeter-structs.h"


/* ****************************************************************************
-- Module Specific #defines
-- ****************************************************************************/

#define TX_MSG_LEN            7
#define VERSION_NUMBER        2

/* ****************************************************************************
-- Local Global Variables
-- ****************************************************************************/

// Variable for measurement transmissions
static uint8_t tx_buf[TX_MSG_LEN];
static uint8_t tx_data_count;
static uint8_t tx_msgid;

/* ****************************************************************************
-- Exported Global Variables
-- ****************************************************************************/

/*! \system configuration */    
int16_t gSysconf;
/*! \AFE functions configuration */    
int16_t gCsgconf;
/*! \power constant for pulse*/    
int16_t gPower_const;
/*! \start current */    
int16_t gStart_curr;
/* Meter status flag bits. */
uint16_t meter_status;
/* energy pulse threshold */
int32_t gCurrent_threshold;

/* The main per-phase working parameter structure */
struct phase_parms_s chan1;

/* The main per-phase non-volatile parameter structure */
__infomem__ const struct info_mem_s nv_parms =
{
    {//seg a
    {//phase_nv_parms_s 
        {//phase_nv_parms_s chan1
            {//current_sensor_nv_parms_s live
                0,
                DEFAULT_I_RMS_SCALE_FACTOR_A,
                DEFAULT_P_SCALE_FACTOR_A_LOW,
                DEFAULT_BASE_PHASE_A_CORRECTION_LOW,
                DEFAULT_P_OFFSET_A_LOW,
                DEFAULT_Q_OFFSET_A_LOW,
            //},
            //{//current_sensor_nv_parms_s neutral
                0,
                DEFAULT_I_RMS_SCALE_FACTOR_NEUTRAL,
                DEFAULT_P_SCALE_FACTOR_NEUTRAL,
                DEFAULT_NEUTRAL_BASE_PHASE_CORRECTION,
                DEFAULT_P_OFFSET_NEUTRAL,  
                DEFAULT_Q_OFFSET_NEUTRAL,  
            },                
            DEFAULT_V_RMS_SCALE_FACTOR_A,
            0,
            DEFAULT_I_DC_ESTIMATE << 16,
            DEFAULT_I_DC_ESTIMATE_NEUTRAL << 16,
            DEFAULT_V_DC_ESTIMATE << 16,
        },
        {
          0,
          0,
        },
        
        0x0004,//sysconf
        0x1003,//afeconf
        1600,//power constant
        0,//start current 0mA

//TEMPERATURE_SUPPORT
        25,
        DEFAULT_TEMPERATURE_OFFSET,
        DEFAULT_TEMPERATURE_SCALING,
//CORRECTED_RTC_SUPPORT
        0,
//rtc
        {
            0,
            0,
            0,
            0,
            0,
            0,
            0
        },
//serial number        
        "",
//property number        
        "",
//factory number        
        ""
    }
    }
};

/* ****************************************************************************
-- Module Prototypes
-- ****************************************************************************/

void InitSendMeasurements();
void SendMeasurements();

/* ****************************************************************************
-- Function Name  : Timer_A()
-- Description    : The entry point for this program.
-- Notes          : None.
-- ****************************************************************************/

#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
  CCR0 += 64000;                            // Add Offset to CCR0
}

/* ****************************************************************************
-- Function Name  : serial_tx_interrupt0()
-- Description    : Causes the serial data to be transmitted.
-- Notes          : None.
-- ****************************************************************************/

#pragma vector=USART0TX_VECTOR
__interrupt void serial_tx_interrupt0(void) {
  TXBUF0=tx_buf[tx_data_count++];
  if (tx_data_count>=TX_MSG_LEN) {
    IE1 &= ~UTXIE0;            
  }      
}

/* ****************************************************************************
-- Function Name  : main()
-- Description    : The entry point for this program.
-- Notes          : None.
-- ****************************************************************************/

void main(int argc, char *argv[])
{
uint8_t ledScale;
  //initiate meter working parameters    
  data_init();
  csg_config();

  //config hardware
  system_setup();
  InitSendMeasurements();
         
  for (;;)
  {
    kick_watchdog();
    if ((phase->status & ZERO_CROSSING)) 
    {
      phase->status &= ~ZERO_CROSSING;    
      SendMeasurements();
    }
    if ((phase->status & NEW_LOG))   
    {
      /* The background activity has informed us that it is time to
      perform a block processing operation. */                
      phase->status &= ~NEW_LOG;    
      ledScale++;
      if (ledScale == 1) 
      {
        P2OUT &= 0xfe;         // Turn LED on
      }
      else if (ledScale == 5) 
      {
        P2OUT |= 0x01;         // Turn LED off
      }
      else if (ledScale > 25) 
      {
        ledScale = 0;
      }
    }
      
    /* Do display and other housekeeping here */
    if ((meter_status & TICKER))
    {
      /* Two seconds have passed */
      /* We have a 2 second tick */
      meter_status &= ~TICKER;
    }
  }
}

/* ****************************************************************************
-- Function Name  : SendMeasurements()
-- Description    : This procedure sends the measured data.
-- Notes          : It first sends the already prepared message and then works
--                  out the next message, done so that the latency is reduced.
-- ****************************************************************************/

void SendMeasurements() 
{
int16_t i;
int32_t x;
  // Send off the ready buffer
  tx_data_count = 0;
  IE1|=UTXIE0;
  // Wait for the buffer to have been transmitted, main() does not really have anything else to do
  while (tx_data_count != TX_MSG_LEN);
  // Set the message ID and Sync indication in bit 7
  tx_buf[1] = tx_msgid | phase->NoZeroCrossing;
  // do our stuff
  if ((tx_msgid & 0x01) == 0)  //Send the RMS voltage
  {
    tx_buf[1] = phase->NoZeroCrossing; // Update the MSG type since it is aliased 0,2,4,6,8,10
    // Calculate the RMS voltage in 10mV increments. Return 0xffff for overrange (i.e. ADC clip).
    if ((phase->status & V_OVERRANGE)) {
      x = 0xffffffff;
    }
    else 
    {
      x = div_sh48(phase->V_sq_accum_logged, 26 - 2*ADC_BITS, phase->sample_count_logged);  
      x = isqrt32(x);
      x = (x >> 12)*phase_nv->V_rms_scale_factor;
      x >>= 14;
    }
  }
  else //It will be one of the interleaved parameters
  {
    switch(tx_msgid) 
    {
      case 1: // The RMS Current
        x = div48(phase->current[0].I_sq_accum_logged[0], phase->sample_count_logged);
        x = isqrt32(x);             
        x = (x >> 8)*phase_nv->current[0].I_rms_scale_factor[0];
        x >>= 14;
        x = x - phase_nv->current[0].Ac_offset;
        // if there is an offset adjustment do it here => x = x - phase_nv->current[0].Ac_offset;
        if (x < 0)
          x = 0;        
        break;
        
      case 3: // The Active Power
        x = div_sh48(phase->current[0].P_accum_logged[0], 24 - 2*ADC_BITS, phase->sample_count_logged);
        i = phase_nv->current[0].P_scale_factor[0];
        x = mul48(x, i);
        x -= phase_nv->current[0].Offset_active_power;
        if (x < 0)  
        {
          x = -x;  
          phase->status |= I_REVERSED;  
        } 
        else 
        {
          phase->status &= ~I_REVERSED;   
        }
        phase->active_power[0] = x;      
        break;
        
      case 5: // Frequency
        x = phase->mains_period;        
        x = (int32_t) SAMPLES_PER_10_SECONDS*256L*10L/(x >> 16);      
        break;
        
      case 7: // Version number
        x = VERSION_NUMBER;
        break;
        
      case 9: //Zero crossing cycle counter
        x = phase->zero_crossings;
        break;
        
      case 11: // Watt Hour indication
        x = phase->energy.total_consumed_energy;
        break;
        
      default:
        break;
    }
  }
  // repack the data
  tx_buf[5] = x;
  x >>= 8;
  tx_buf[4] = x;
  x >>= 8;
  tx_buf[3] = x;
  x >>= 8;
  tx_buf[2] = x;
  // Select the next message ID
  if (tx_msgid >= 11) 
  {
    tx_msgid=0;
  }
  else 
  {
    tx_msgid++;  
  }
  // Calculate the check
  tx_buf[6] = 0;
  for (i = 0; i < 6; i++) 
  {
    tx_buf[6] += tx_buf[i];
  }  
}

/* ****************************************************************************
-- Function Name  : InitSendMeasurements()
-- Description    : Inits the global variabled used by SendMeasurements().
-- Notes          : None.
-- ****************************************************************************/

void InitSendMeasurements() 
{
  tx_buf[0] = 0xa5; // Message ID, hard coded 0xa5
  tx_msgid = 0;
  tx_data_count = TX_MSG_LEN;
}



