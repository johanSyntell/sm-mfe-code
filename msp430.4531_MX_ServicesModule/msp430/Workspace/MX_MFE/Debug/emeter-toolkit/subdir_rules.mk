################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
emeter-toolkit/dc_filter.obj: ../emeter-toolkit/dc_filter.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/dc_filter.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/div48.obj: ../emeter-toolkit/div48.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/div48.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/div_sh48.obj: ../emeter-toolkit/div_sh48.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/div_sh48.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/imul16.obj: ../emeter-toolkit/imul16.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/imul16.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/isqrt32.obj: ../emeter-toolkit/isqrt32.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/isqrt32.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/mul48.obj: ../emeter-toolkit/mul48.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/mul48.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

emeter-toolkit/q1_15_mul.obj: ../emeter-toolkit/q1_15_mul.c $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/bin/cl430" -vmsp --abi=eabi -g --include_path="C:/Program Files/ti/ccsv5/ccs_base/msp430/include" --include_path="C:/Program Files/ti/ccsv5/tools/compiler/msp430_4.0.2/include" --define=__MSP430AFE232__ --diag_warning=225 --display_error_number --printf_support=minimal --preproc_with_compile --preproc_dependency="emeter-toolkit/q1_15_mul.pp" --obj_directory="emeter-toolkit" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


