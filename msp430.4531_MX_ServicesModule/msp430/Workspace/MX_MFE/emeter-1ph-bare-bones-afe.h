//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  copyright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//    File: emeter-1ph-stefan.h
//  Stefan's FE427 reference board
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter.h,v 1.26 2005/12/20 10:17:58 a0754793 Exp $
//
//--------------------------------------------------------------------------

/* This is a demonstration version of the configuration data file for the
   MSP430 e-meter reference software. For most users, this file will need
   to be edited to match the hardware being used. */

/* N.B.
   This e-meter software uses a sampling rate of 3276.8/s. For a
   mains supply at exactly 50Hz, each sample is 5.49degrees of
   phase from the last. */

/*! This switch enables the sending of the current readings, through a UART port,
    for use in cooperative calibration with other meters. */
#define SERIAL_CALIBRATION_REF_SUPPORT              1

/*! This switch selects single phase mode. If SINGLE_PHASE is not set the
    software will be built for 3-phase operation. */
#define SINGLE_PHASE                                1

/*! This selects support for reactive power measurement. */
#define REACTIVE_POWER_SUPPORT                      0

/*! This selects support for reactive power measurement through quadrature processing.
    This is only effective when REACTIVE_POWER_SUPPORT is enabled. */
#define REACTIVE_POWER_BY_QUADRATURE_SUPPORT

/*! The selects support for apparent or VA power measurement. */
#undef APPARENT_POWER_SUPPORT
#define APPARENT_POWER_SUPPORT

/*! This switch enables support for power factor measurement. This feature
    includes a lead/lag assessment. A frequency independant method, based
    on the ratio of scalar dot products, is used. */
#undef POWER_FACTOR_SUPPORT
//#define POWER_FACTOR_SUPPORT

/*! This switch selects support for measuring the total active energy consumption. */
#define TOTAL_ENERGY_SUPPORT                        1

/*! This switch selects support for measuring the total reactive energy. */
#define TOTAL_REACTIVE_ENERGY_SUPPORT               0

/*! This switch enables mains frequency measurement. This may be used as a
    meter feature. It may be a requirement, if non-linear CT sensors are used. */
#define MAINS_FREQUENCY_SUPPORT                     1

/*! This selects the nominal voltage used for power calculations in limp mode */
#define MAINS_NOMINAL_VOLTAGE                       230

/*! The nominal mains frequency is used to prime the mains frequency measurement,
    and make its initial value settle quickly. It is not currently used after
    reset. */
#define MAINS_NOMINAL_FREQUENCY                     50

/*! This sets the number of pulses per kilo-watt hour the meter will produce at
    its total energy pulse LED. It does not affect the energy accumulation process. */
#define TOTAL_ENERGY_PULSES_PER_KW_HOUR             1600

/*! This switch inhibits the accumulation of total negative power */
#undef INHIBIT_NEGATIVE_TOTAL_POWER_ACCUMULATION

/*! The duration of the LED on time for an energy pulse. This is measured in
    ADC samples (i.e. increments 1/3276.8s). The maximum allowed is 255, giving a
    pulse of about 78ms. 163 gives a 50ms pulse. */
#define ENERGY_PULSE_DURATION                       163

/*! This switch enables monitoring of the neutral lead for anti-fraud purposes. */
//#define NEUTRAL_MONITOR_SUPPORT                     1
#undef NEUTRAL_MONITOR_SUPPORT

///////////////////////////////////Pulse///////////////////////////////////////
/*! This is called to turn on a total energy pulse indicator (e.g. LED or LCD segment) */
#define set_total_energy_pulse_indicator()          //(P1OUT &= ~BIT1)//(P1OUT |= BIT3)

/*! This is called to turn off a total energy pulse indicator (e.g. LED or LCD segment) */
#define clr_total_energy_pulse_indicator()          //(P1OUT |= BIT1)//(P1OUT &= ~BIT3)

/*! This is called to turn on a total reactive energy pulse indicator (e.g. LED or LCD segment) */
#define set_total_reactive_energy_pulse_indicator() //(P1OUT &= ~BIT0)//(P1OUT |= BIT4)/**/

/*! This is called to turn off a total reactive energy pulse indicator (e.g. LED or LCD segment) */
#define clr_total_reactive_energy_pulse_indicator() //(P1OUT |= BIT0)//(P1OUT &= ~BIT4)/**/

///////////////////////////////////Imbalance detect//////////////////////////////////
/*! This is a shift value for comparing currents or powers when looking for
    imbalance between live and neutral. 3 give 12.5%. 4 give 6.25%. These are the
    two commonest values to use. The relaxed version is used at low power levels,
    where the values are less accurate, and a tight imbalance check might give false
    results. */
#define PERMITTED_IMBALANCE_FRACTION                4

/*! This is a relaxed version of the permitted imbalance fraction, for use at low
    powers/currents, where the power (and hence imbalance) measurement may be less
    precise. The tighter imbalance measurement may give false results under high
    EMI conditions. */
#define RELAXED_IMBALANCE_FRACTION                  2

/*! This is the number of successive measurement blocks which must agree the
    unbalanced status has changed before we accept it. */
#define PHASE_UNBALANCED_PERSISTENCE_CHECK          5

/*! This is the minimum current level (limp mode) and minimum power level (normal
    mode) at which we will make checks for the earthed condition. Very small
    readings lack the accuracy and resolution needed to make meaningfulF comparisons
    between live and neutral. */
#define PHASE_UNBALANCED_THRESHOLD_CURRENT          500
#define PHASE_UNBALANCED_THRESHOLD_POWER            2000


////////////////////Power down and Limp Mode Support/////////////////////////
/*! This selects the operation from current only, when only one lead is
    functioning, and the meter is powered by a parasitic CT supply attached to
    the leads. This is for anti-fraud purposes. Energy is accumulated at the
    highest possible rate, assuming unity power factor, and the nominal voltage */
//#define LIMP_MODE_SUPPORT                         1

/*! If limp mode is supported, these set the voltage thresholds for switching
    bewteen normal mode and limp mode. */
//#define LIMP_MODE_VOLTAGE_THRESHOLD               50
//#define NORMAL_MODE_VOLTAGE_THRESHOLD             80

/*! If limp mode is supported, this sets the threshold current in mA, below which we
    no not operate. */
//#define LIMP_MODE_MINIMUM_CURRENT

/////////////////////////////serial port support////////////////////////////////
/*! This switch, in combination with the calibrator switch, enables calibration
    with the meter cooperating with an external reference, through a UART port. */
#define SERIAL_CALIBRATION_SUPPORT
//#undef SERIAL_CALIBRATION_SUPPORT
#if !defined(SERIAL_CALIBRATION_PASSWORD_1)
#define SERIAL_CALIBRATION_PASSWORD_1               0x1234
#define SERIAL_CALIBRATION_PASSWORD_2               0x5678
#define SERIAL_CALIBRATION_PASSWORD_3               0x9ABC
#define SERIAL_CALIBRATION_PASSWORD_4               0xDEF0
#endif

/*! This switch enables support of an IR receiver and/or transmitter for
    programming and/or reading the meter. */
#undef IEC1107_SUPPORT
#undef IEC62056_21_SUPPORT
#undef DLT645_SUPPORT

/*! This defines the speed of UART 0 */
#define USART0_BAUD_RATE                            9600

/*! Normally the meter software only calculates the properly scaled values
    for voltage, current, etc. as these values are needed. This define
    enables additional global parameters, which are regularly updated with
    all the metrics gathered by the meter. This is generally less efficient,
    as it means calculating things more often than necessary. However, some
    may find this easier to use, so it is offered as a choice for the meter
    designer. */
#define PRECALCULATED_PARAMETER_SUPPORT             1

//////////////////////////////////////configuration/////////////////////////////
/*! The gain setting for the first current channel channel of the SD24,
    for devices using the SD24 sigma-delta ADC.
    This must be set to suit the shunt or CT in use. Typical values for a
    shunt are GAIN_16 (x16 gain) or GAIN_32 (x32 gain). Typical values for a
    CT are GAIN_1 (x1 gain) or GAIN_2 (x2 gain). */
#define CURRENT_LIVE_GAIN                           SD24GAIN_1//SD24GAIN_8//SD24GAIN_16

/*! The gain setting for the second current channel channel of the SD24,
    for devices using the SD24 sigma-delta ADC.
    This must be set to suit the shunt or CT in use. Typical values for a
    shunt are GAIN_16 (x16 gain) or GAIN_32 (x32 gain). Typical values for a
    CT are GAIN_1 (x1 gain) or GAIN_2 (x2 gain). */
#define CURRENT_NEUTRAL_GAIN                        SD24GAIN_1

/*! The gain setting for the voltage channel of the SD24, for devices using the
    SD24 sigma-delta ADC. This is usually GAIN_1 (i.e. x1 gain). */
#define VOLTAGE_GAIN                                SD24GAIN_1

/*! The following allows the live and neutral inputs to be swapped. The usual
    arrangement if that current channel 1 (input 0) is live and current channel
    2 (input 1) is neutral. If CURRENT_CH_2_IS_LIVE is defined, these inputs are
    reversed. */
#define CURRENT_CH_2_IS_LIVE
#undef CURRENT_CH_2_IS_LIVE

#define GAIN_STAGES                                 1
#define NEUTRAL_GAIN_STAGES                         1
#define I_HISTORY_STEPS                             1

///////////////////////////////////reverse current monitor/////////////////////
/*! This is the number of successive measurement blocks which must agree the
    reversed current status has changed before we accept it. */
#define PHASE_REVERSED_PERSISTENCE_CHECK            5

/*! This is the minimum power level at which we will make checks for the reverse
    condition. Very small readings are not a reliable indicator, due to noise. */
#define PHASE_REVERSED_THRESHOLD_POWER              2000

#define DEFAULT_V_RMS_SCALE_FACTOR_A                13211
#define DEFAULT_I_RMS_SCALE_FACTOR_A                1076
#define DEFAULT_P_SCALE_FACTOR_A_LOW                2218    /* Low current, high gain, range. */
#define DEFAULT_P_OFFSET_A_LOW                      0
#define DEFAULT_Q_OFFSET_A_LOW                      0
#define DEFAULT_BASE_PHASE_A_CORRECTION_LOW         0//(256-40)

#define DEFAULT_VOLTAGE_PRELOAD                     12//72   //initiation preload for voltage channel

#define DEFAULT_I_RMS_SCALE_FACTOR_NEUTRAL          3949 //pktbr
#define DEFAULT_P_SCALE_FACTOR_NEUTRAL              8876
#define DEFAULT_P_OFFSET_NEUTRAL                    0
#define DEFAULT_Q_OFFSET_NEUTRAL                    0
#define DEFAULT_NEUTRAL_BASE_PHASE_CORRECTION       0//264//0xfa

#define INTERRUPT_BIT   BIT5
//set for uart
#define P1DIR_INIT      (BIT0 | BIT1 | BIT3)
#define P1SEL_INIT      (BIT3 | BIT4)//UTXD0, URXD0
#define P1OUT_INIT      (BIT0 | BIT1)

#define P2DIR_INIT      BIT0
#define P2SEL_INIT      (BIT6+BIT7)     
#define P2OUT_INIT      0

#define EVARAGE_POUT
#undef EVARAGE_POUT

#ifdef  EVARAGE_POUT
#define EVARAGE_THRESHOLD 10000
#define EVARAGE_FILTER_DEPTH 4
#endif

//#define DCRMV_I
//#define DCRMV_V
//#undef  DCRMV_I
//#undef  DCRMV_V

#define REALTIME_PULSE
#undef REALTIME_PULSE

#define USE_WATCHDOG            1
#define DEFAULT_ENERGY_PULSE_THRESHOLD          (unsigned long)0x1BE000

#define DEFAULT_TEMPERATURE_OFFSET                  (1615*8)
#define DEFAULT_TEMPERATURE_SCALING                 (704*2)
