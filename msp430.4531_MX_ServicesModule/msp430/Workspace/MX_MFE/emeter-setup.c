//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY, 
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS 
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR 
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE. 
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET 
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY 
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR 
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL, 
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY 
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED 
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT 
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM. 
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF 
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS 
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF 
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S 
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF 
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS 
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted 
//  by Texas Instruments is distributed as "freeware".  You may, 
//  only under TI's copyright in the Program, use and modify the 
//  Program without any charge or restriction.  You may 
//  distribute to third parties, provided that you transfer a 
//  copy of this license to the third party and the third party 
//  agrees to these terms by its first use of the Program. You 
//  must reproduce the copyright notice and any other legend of 
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains 
//  copyrighted material, trade secrets and other TI proprietary 
//  information and is protected by copyright laws, 
//  international copyright treaties, and trade secret laws, as 
//  well as other intellectual property laws.  To protect TI's 
//  rights in the Program, you agree not to decompile, reverse 
//  engineer, disassemble or otherwise translate any object code 
//  versions of the Program to a human-readable form.  You agree 
//  that in no event will you alter, remove or destroy any 
//  copyright notice included in the Program.  TI reserves all 
//  rights not specifically granted under this license. Except 
//  as specifically provided herein, nothing in this agreement 
//  shall be construed as conferring by implication, estoppel, 
//  or otherwise, upon you, any license or other right under any 
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-setup.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-setup.c,v 1.30 2008/10/28 10:13:40 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
//  MSP430 setup routines for e-meters.
//
//  This software is appropriate for single phase e-meters
//  using a voltage sensor plus CT or shunt resistor current sensors, or
//  a combination of a CT plus a shunt. 
//
#include <stdint.h>
#include <string.h>

#if defined(__GNUC__)
#include <signal.h>
#endif
#include <msp430afe253.h>
#include "emeter-toolkit\\emeter-toolkit.h"

#include "emeter-structs.h"

#define SD24CONF0_FUDGE     0x70
#define SD24CONF1_FUDGE     0xF8    //0x38

/*
 * Analog front-end initialization routine.
 *
 * Configures the sigma-delta ADC module as analog front-end for
 * a tamper-resistant meter using a current transformer and a
 * shunt as current sensors (see configuration of channel 0 and 1).
 */
static __inline__ void init_analog_front_end_normal(void)
{
    /*
     * First it makes sure that the Embedded Signal Processing is 
     * disabled, otherwise it will not be possible to modify the 
     * SD24 registers.
     */
    #if defined(__MSP430_E423__)  ||  defined(__MSP430_E425__)  ||  defined(__MSP430_E427__)
    ESPCTL &= ~ESPEN;
    #endif

    /*
     * Then the general configurations of the analog front-end are done
     * that apply to all channels: clock selection (SMCLK) and divider
     * settings (depending on SMCLK frequency) and reference voltage
     * selections.
     */

    SD24CCTL_VOLTAGE &= ~SD24SC;
    SD24CCTL_LIVE &= ~SD24SC;
    #if defined(NEUTRAL_MONITOR_SUPPORT)
    SD24CCTL_NEUTRAL &= ~SD24SC;
    #endif

    SD24CTL = SD24SSEL_1  /* Clock is SMCLK */
            | SD24DIV_3   /* Divide by 8 => ADC clock: 1MHz */
            | SD24REFON;  /* Use internal reference */

    SD24INCTL_LIVE = SD24INCH_CURRENT | CURRENT_LIVE_GAIN;         /* Set gain for channel 0 (I1) */
    SD24CCTL_LIVE = SD24OSR_256 | SD24DF | SD24GRP |SD24IE;        /* Set oversampling ratio to 256 (default) */
    SD24PRE_LIVE = DEFAULT_BASE_PHASE_A_CORRECTION_LOW;

        #if defined(NEUTRAL_MONITOR_SUPPORT)
    SD24INCTL_NEUTRAL = SD24INCH_CURRENT | (gSysconf&0x0038);      /* Set gain for channel 1 (I2) */
    SD24CCTL_NEUTRAL = SD24OSR_256 | SD24DF | SD24GRP | SD24IE;    /* Set oversampling ratio to 256 (default) */
    SD24PRE_NEUTRAL = 0;
        #endif

    /* Configure analog front-end channel 2 - Voltage */
    SD24INCTL_VOLTAGE = SD24INCH_0 | ((gSysconf&0x01C0)>>3);       /* Set gain for channel 2 (V) */
    SD24CCTL_VOLTAGE = SD24OSR_256 | SD24DF | SD24SC | SD24IE;     /* Set oversampling ratio to 256 (default) */
    SD24PRE_VOLTAGE = DEFAULT_VOLTAGE_PRELOAD;

    #if defined(SD24CONF0_FUDGE)
//pk    SD24CONF0 = SD24CONF0_FUDGE;                /* Tweaks recommended by Freising */
    #endif
    #if defined(SD24CONF1_FUDGE)
    SD24CONF1 = SD24CONF1_FUDGE;
    #endif

    chan1.current[0].in_phase_correction[0].sd16_preloaded_offset = 0;
        #if defined(NEUTRAL_MONITOR_SUPPORT)
    chan1.current[1].in_phase_correction[0].sd16_preloaded_offset = 0;
        #endif
    /*
     * \note 
     * Please note, the oversampling ratio should be the same 
     * for all channels. Default is 256.
     */
}

static __inline__ void disable_analog_front_end(void)
{
    #if defined(__MSP430_E427__)
    ESPCTL &= ~ESPEN;
    #endif

    SD24INCTL_VOLTAGE = 0;
    SD24CCTL_VOLTAGE = 0;
    SD24PRE_VOLTAGE = 0;

    SD24INCTL_LIVE = 0;
    SD24CCTL_LIVE = 0;
    SD24PRE_LIVE = 0;

    #if defined(NEUTRAL_MONITOR_SUPPORT)
    SD24INCTL_NEUTRAL = 0;
    SD24CCTL_NEUTRAL = 0;
    SD24PRE_NEUTRAL = 0;
    #endif

    SD24CTL = 0;
    #if defined(SD24CONF0_FUDGE)
//pk    SD24CONF0 = SD24CONF0_FUDGE;
    #endif
    #if defined(SD24CONF1_FUDGE)
    SD24CONF1 = SD24CONF1_FUDGE;
    #endif

    chan1.current[0].in_phase_correction[0].sd16_preloaded_offset = 0;
        #if defined(NEUTRAL_MONITOR_SUPPORT)
    chan1.current[1].in_phase_correction[0].sd16_preloaded_offset = 0;
        #endif
        
    chan1.energy.total_active_power_counter = 0;
    chan1.energy.total_consumed_energy = 0;
}

void system_setup(void)
{
    /* Set up the DCO clock */
    volatile unsigned int i=0;
    WDTCTL = (WDTCTL & 0xFF) | WDTPW | WDTHOLD;
    BCSCTL1 &= ~XT2OFF;                       // Activate XT2 high freq xtal
    BCSCTL3 |= XT2S_2+LFXT1S_2;               // 3 � 16MHz crystal or resonator
    do
    {
      IFG1 &= ~OFIFG;                         // Clear OSCFault flag
      for (i = 0xFFF; i > 0; i--);            // Time for flag to set
    }
    while (IFG1 & OFIFG);                     // OSCFault flag still set?
    BCSCTL2 |= SELS+SELM_2;                   // MCLK = XT2 HF XTAL (safe)
    P1DIR |= BIT0+BIT1;                       // P1.1 = output direction
    P1SEL |= BIT0;
    P1SEL2 |= BIT0;

    WDTCTL = (WDTCTL & 0xFF) | WDTPW | WDTHOLD;
    //IE1 |= WDTIE;     /* Enable the WDT interrupt */

    #if defined(P1OUT_INIT)
    P1OUT = P1OUT_INIT;
    #endif
    #if defined(P1DIR_INIT)
    P1DIR = P1DIR_INIT;
    #endif
    #if defined(P1SEL_INIT)
    P1SEL = P1SEL_INIT;
    #endif

    #if defined(P2OUT_INIT)
    P2OUT = P2OUT_INIT;
    #endif
    #if defined(P2DIR_INIT)
    P2DIR = P2DIR_INIT;
    #endif
    #if defined(P2SEL_INIT)
    P2SEL = P2SEL_INIT;
    #endif

    disable_analog_front_end();

    /////////////init usart as uart mode////////////////
    U0CTL = CHAR+SWRST;		// 8-bit, UART   
    U0TCTL = SSEL1 + TXEPT;
    ME1 |= UTXE0 + URXE0;	// Module enable
    UBR00 = 0x41;                 // 8,000,000/9600 = 833 = 0x0341
    UBR10 = 0x03;                            
    UMCTL0 = 0x00;              // Modulation
    UCTL0 &= ~SWRST; 
    IE1 |= URXIE0;              // 5		
   
      #if defined(TOTAL_ENERGY_SUPPORT)
    clr_total_energy_pulse_indicator();
      #endif
    clr_total_reactive_energy_pulse_indicator();

    /* Prime the DC estimates for quick settling */
    phase->current[0].I_dc_estimate[0] = phase_nv->initial_I_dc_estimate[0];
    phase->current[0].I_endstops = ENDSTOP_HITS_FOR_OVERLOAD;
      #if defined(NEUTRAL_MONITOR_SUPPORT)
    phase->current[1].I_dc_estimate[0] = phase_nv->initial_I_dc_estimate[1];
    phase->current[1].I_endstops = ENDSTOP_HITS_FOR_OVERLOAD;
      #endif
    phase->V_dc_estimate = phase_nv->initial_v_dc_estimate;
    phase->V_endstops = ENDSTOP_HITS_FOR_OVERLOAD;
      #if defined(MAINS_FREQUENCY_SUPPORT)
    phase->mains_period = ((SAMPLES_PER_10_SECONDS*6554)/MAINS_NOMINAL_FREQUENCY) << 8;
      #endif   

    phase->zero_crossings = 0;      
   
    _EINT();
  
        #if defined(__MSP430_HAS_SVS__)
    /* Before we go to high speed we need to make sure the supply voltage is 
       adequate. If there is an SVS we can use that. There should be no wait
       at this point, since we should only have been woken up if the supply
       is healthy. However, it seems better to be cautious. */
    SVSCTL = (SVSON | 0x20);
    /* Wait for adequate voltage to run at full speed */
    while ((SVSCTL & SVSOP))
        /* dummy loop */;
    /* The voltage should now be OK to run the CPU at full speed. Now it should
       be OK to use the SVS as a reset source. */
    SVSCTL |= PORON;
        #endif

    kick_watchdog();
    switch_to_normal_mode();
}

void switch_to_normal_mode(void)
{
    /* Switch to full speed, full power mode */
        #if defined(__MSP430_HAS_TA3__)
    /* Disable the TIMER_A0 interrupt */
    TACTL = 0;
    TACCTL0 = 0;
        #endif
       
    _DINT();
    init_analog_front_end_normal();
//    CCTL0 = CCIE;                             // CCR0 interrupt enabled
//    CCR0 =64000;
    TACTL = TASSEL_2 + MC_2;                  // SMCLK, contmode

    _EINT();
}

void data_init(void)
{
/*! \system configuration */    
  gSysconf = p_parms_nv->sysconf;
/*! \AFE functions configuration */    
  gCsgconf = p_parms_nv->csgconf;
/*! \power constant for pulse*/    
  gPower_const = p_parms_nv->power_const;
/*! \start current */    
  gStart_curr = p_parms_nv->start_curr; 
/*! \csg local status */      
  meter_status = 0;
}

void csg_config(void)
{
unsigned char shiftbit = 0;
  gCurrent_threshold = TOTAL_ENERGY_PULSE_THRESHOLD;

  if(gPower_const > 1000)  //in normal power counter mode, multiply 16 times to compensate the shiftright while defination of TOTAL_ENERGY_PULSE_THRESHOLD
  gCurrent_threshold = gCurrent_threshold*16;
  gCurrent_threshold = gCurrent_threshold/gPower_const;
  gCurrent_threshold = gCurrent_threshold*1600;
  
  if((gCsgconf&CSGCON_FPEN)==CSGCON_FPEN){  //fast pulse mode enabled, adjust threshold with fast pulse factor
    shiftbit = (gCsgconf&CSGCON_FPFACTOR_MASK)>>2;
    gCurrent_threshold = gCurrent_threshold>>shiftbit;
  }

  if((gCsgconf&CSGCON_CSEL_MASK)==CSGCON_CSEL2)//channel B mode                      
    phase->status |= CURRENT_FROM_NEUTRAL;
  if((gCsgconf&CSGCON_CSEL_MASK)==CSGCON_CSEL1)//channel A mode                      
    phase->status &= ~CURRENT_FROM_NEUTRAL;
  
}
