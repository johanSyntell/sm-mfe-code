################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../emeter-toolkit/dc_filter.c \
../emeter-toolkit/div48.c \
../emeter-toolkit/div_sh48.c \
../emeter-toolkit/imul16.c \
../emeter-toolkit/isqrt32.c \
../emeter-toolkit/mul48.c \
../emeter-toolkit/q1_15_mul.c 

OBJS += \
./emeter-toolkit/dc_filter.obj \
./emeter-toolkit/div48.obj \
./emeter-toolkit/div_sh48.obj \
./emeter-toolkit/imul16.obj \
./emeter-toolkit/isqrt32.obj \
./emeter-toolkit/mul48.obj \
./emeter-toolkit/q1_15_mul.obj 

C_DEPS += \
./emeter-toolkit/dc_filter.pp \
./emeter-toolkit/div48.pp \
./emeter-toolkit/div_sh48.pp \
./emeter-toolkit/imul16.pp \
./emeter-toolkit/isqrt32.pp \
./emeter-toolkit/mul48.pp \
./emeter-toolkit/q1_15_mul.pp 

C_DEPS__QUOTED += \
"emeter-toolkit\dc_filter.pp" \
"emeter-toolkit\div48.pp" \
"emeter-toolkit\div_sh48.pp" \
"emeter-toolkit\imul16.pp" \
"emeter-toolkit\isqrt32.pp" \
"emeter-toolkit\mul48.pp" \
"emeter-toolkit\q1_15_mul.pp" 

OBJS__QUOTED += \
"emeter-toolkit\dc_filter.obj" \
"emeter-toolkit\div48.obj" \
"emeter-toolkit\div_sh48.obj" \
"emeter-toolkit\imul16.obj" \
"emeter-toolkit\isqrt32.obj" \
"emeter-toolkit\mul48.obj" \
"emeter-toolkit\q1_15_mul.obj" 

C_SRCS__QUOTED += \
"../emeter-toolkit/dc_filter.c" \
"../emeter-toolkit/div48.c" \
"../emeter-toolkit/div_sh48.c" \
"../emeter-toolkit/imul16.c" \
"../emeter-toolkit/isqrt32.c" \
"../emeter-toolkit/mul48.c" \
"../emeter-toolkit/q1_15_mul.c" 


