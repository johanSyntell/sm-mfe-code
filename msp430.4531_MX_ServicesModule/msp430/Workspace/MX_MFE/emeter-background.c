//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  copyright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-background.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-background.c,v 1.39 2008/10/28 10:13:40 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
//  MSP430 background (interrupt) routines for e-meters
//
//  This software is appropriate for single phase e-meters
//  using a voltage sensor plus a CT or shunt resistor current sensors, or
//  a combination of a CT plus a shunt.
//
//    The background process deals with the input samples.
//    These are first stored into buffers.
//    The buffered samples are processed as follows:
//    -Voltage and current signals are converted to DC-less AC signals
//    -The current signal is phase compensated
//    -Voltage and current are signed multiplied to give power.
//    -Power samples are accumulated. The accumulated power samples are averaged (in foreground.c)
//     after a number of voltage cycles has been detected.
//
#include <stdint.h>
#include <stdlib.h>

#include <msp430afe253.h>
#include "emeter-toolkit\\emeter-toolkit.h"
#include "emeter-structs.h"


int32_t peters32;
int16_t pcount;
int32_t paccum;

static void __inline__ log_parameters(void)
{

#define i 0

    /* Take a snapshot of various values for logging purposes; tell the
       foreground to deal with them; and clear the working values ready
       for the next analysis period. */
    if (phase->V_endstops <= 0)
        phase->status |= V_OVERRANGE;
    else
        phase->status &= ~V_OVERRANGE;
    phase->V_endstops = ENDSTOP_HITS_FOR_OVERLOAD;

    transfer48(phase->V_sq_accum_logged, phase->V_sq_accum);

    if (phase->current[0].I_endstops <= 0)
        phase->status |= I_OVERRANGE;
    else
        phase->status &= ~I_OVERRANGE;
    phase->current[0].I_endstops = ENDSTOP_HITS_FOR_OVERLOAD;

    transfer48(phase->current[0].I_sq_accum_logged[i], phase->current[0].I_sq_accum[i]);

    transfer48(phase->current[0].P_accum_logged[i], phase->current[0].P_accum[i]);
    transfer48(phase->current[0].P_reactive_accum_logged[i], phase->current[0].P_reactive_accum[i]);

    phase->sample_count_logged = phase->sample_count;
    phase->sample_count = 0;

    /* Tell the foreground there are things to process. */
    phase->status |= NEW_LOG;

#undef i

}

/*---------------------------------------------------------------------------
  This is the main interrupt routine where the main signal processing is done
  ---------------------------------------------------------------------------*/
//ISR(SD24, adc_interrupt)
#pragma vector=SD24_VECTOR
__interrupt void adc_interrupt(void)
{
    int16_t V_sample;
    int16_t corrected;
    int16_t I_live_sample;


#define i 0
#define use_stage 0

    static int16_t adc_buffer[3];
    int adc_ptr;
    
#if defined(MAINS_FREQUENCY_SUPPORT)
    int k;
    int x;
    int y;
    int z;
#endif

    if (!(SD24CCTL_VOLTAGE & SD24IFG))
    {
        /* We do not have a complete set of samples yet, but we may need to pick
           up some current values at this time */
        if ((SD24CCTL_LIVE & SD24IFG))
        {
            adc_buffer[1] = SD24MEM_LIVE;
            SD24CCTL_LIVE &= ~SD24IFG;
        }
        return;
    }

    /* Filter away the DC bias.

       Do the phase lag compensation. Use a simple FIR approach,
       and absorb the non-unity gain of the filter in the overall
       current/power scaling later on. This is OK for the small
       phase shifts we expect to get. It would cause dynamic
       range problems for larger shifts. Note the some of this
       phase shift is due to the operation of the ADC itself. It
       performs sequential conversions of its 8 inputs, so there is
       some time delay between sampling of the various sensors.

       Accumulate power for each of the channels. These will
       be divided by the number of samples at the end of the
       measurement cycles, resulting in an average power
       value for each source.

       If RMS voltage and/or current readings are required, calculate the
       dot products needed to evaluate these. */

    /* Voltage is available */
    adc_buffer[0] = SD24MEM_VOLTAGE;
    SD24CCTL_VOLTAGE &= ~SD24IFG;
    /* Pick up any current samples which may have occurred a little before the
       voltage sample, but not those which may have occurred just after the
       voltage sample. */
    if ((unsigned char)(phase->current[0].in_phase_correction[0].sd16_preloaded_offset & 0xFF) < 128  &&  (SD24CCTL_LIVE & SD24IFG))
    {
        adc_buffer[1] = SD24MEM_LIVE;
        SD24CCTL_LIVE &= ~SD24IFG;
    }
    /* We have a complete set of samples. Process them. */
    adc_ptr = -1;

////////////////////////////////////////////////////////////////////////////////
//////////////////////Now we already got samples, start caculating/////////////
///////////////////////////////////////////////////////////////////////////////
    kick_watchdog();
    {
////////////////////////////////////////////////////////////////////////////////
//////////////////////////Filtering V sample and accumulate V*V/////////////////
        V_sample = adc_buffer[++adc_ptr];
        if ((V_sample >= ADC_MAX  ||  V_sample <= ADC_MIN)  &&  phase->V_endstops)
            phase->V_endstops--;

        accum48(phase->V_sq_accum, imul16(V_sample, V_sample));

        /* We need to save the history of the voltage signal if we are performing phase correction, and/or
           measuring the quadrature shifted power (to obtain an accurate measure of one form of the reactive power). */
        phase->V_history[(int) phase->V_history_index] = V_sample;

////////////////////////////////////////////////////////////////////////////////
/////////////////////////Filtering I sample and accumulate I*I//////////////////
        //IRMS caculation need filtered samples to remove DC offset.
/*        I_live_sample = adc_buffer[1]; //pk dc_filter(&phase->current[0].I_dc_estimate[0], phase->current[0].I_history[0][0]);
        if (I_live_sample < 0) {
          paccum = paccum - I_live_sample;
        }
        else {
          paccum = paccum + I_live_sample;
        }
        pcount ++;
        if (pcount > 4095) {
          pcount = 0;
          peters32 = paccum>>12;
          paccum = 0;
        }*/
        I_live_sample = dc_filter(&phase->current[0].I_dc_estimate[0], phase->current[0].I_history[0][0]);
        
        corrected = adc_buffer[++adc_ptr];

        if ((corrected >= ADC_MAX  ||  corrected <= ADC_MIN)  &&  phase->current[0].I_endstops)
            phase->current[0].I_endstops--;

        phase->current[0].I_history[0][0] = corrected;

        accum48(phase->current[0].I_sq_accum[use_stage], imul16(I_live_sample, I_live_sample));
        
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////accumulate P and Q//////////////////
            /* Perform phase shift compensation, to allow for the time
               between ADC samplings, internal phase shifts in CTs, etc.
               This uses a 1 tap FIR (basically an interpolator/extrapolator) */
        //But power accumulation can not use DC filter because it may induce jittering
#ifndef  DCRMV_I
        I_live_sample = -phase->current[0].I_history[0][0];
#endif
        corrected = phase->V_history[(phase->V_history_index - phase->current[0].in_phase_correction[use_stage].step) & V_HISTORY_MASK];

        accum48(phase->current[0].P_accum[use_stage], imul16(corrected, I_live_sample));

        corrected = (Q1_15_mul(phase->V_history[(phase->V_history_index - phase->current[0].quadrature_correction[use_stage].step - 1) & V_HISTORY_MASK], phase->current[0].quadrature_correction[use_stage].fir_beta) >> 1)
                    + (phase->V_history[(phase->V_history_index - phase->current[0].quadrature_correction[use_stage].step) & V_HISTORY_MASK] >> 1);
        accum48(phase->current[0].P_reactive_accum[use_stage], imul16(corrected, I_live_sample));

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////update sample count/////////////////
        phase->V_history_index = (phase->V_history_index + 1) & V_HISTORY_MASK;
        ++phase->sample_count;
////////////////////////////////////////////////////////////////////////////////
///////////////Frequency and power factor caculation on live///////////////////
//There is a filtering algorithm on frequency caculation to remove sparkle////
        /* Do the power cycle start detection */
        /* There is no hysteresis used here, but since the signal is
           changing rapidly at the zero crossings, and is always of
           large amplitude, miscounting cycles due to general noise
           should not occur. Spikes are another matter. A large spike
           could cause the power cycles to be miscounted, but does not
           matter very much. The cycle counting is not critical to power
           or energy measurement. */
#if defined(MAINS_FREQUENCY_SUPPORT)
        phase->cycle_sample_count += 256;
#endif
        if (abs(V_sample - phase->last_V_sample) <= phase->since_last*MAX_PER_SAMPLE_VOLTAGE_SLEW)
        {
            /* This doesn't look like a spike - do mains cycle detection, and
               estimate the precise mains period */
            if (V_sample < 0)
            {
              if ( (phase->status & V_POS) == V_POS) {
                phase->status |= ZERO_CROSSING;
                phase->zero_crossings++;
                phase->NoZeroCrossing = 0;
                phase->period_time = 0;
              }
              else if (phase->period_time > (SAMPLES_PER_SECONDS/90)) { // check that mains has not collapsed
                phase->period_time = 0;
                phase->status |= ZERO_CROSSING;
                phase->NoZeroCrossing = 0x80;
              }
              /* Log the sign of the signal */
              phase->status &= ~V_POS;
            }
            else
            {
                if (!(phase->status & V_POS))
                {
#if defined(MAINS_FREQUENCY_SUPPORT)
                    /* Apply limits to the sample count, to avoid spikes or dying power lines disturbing the
                       frequency reading too much */
                    /* The mains should be <40Hz or >70Hz to fail this test! */
                    if (256*SAMPLES_PER_10_SECONDS/700 <= phase->cycle_sample_count  &&  phase->cycle_sample_count <= 256*SAMPLES_PER_10_SECONDS/400)
                    {
                        /* A mains frequency measurement procedure based on interpolating zero crossings,
                           to get a fast update rate for step changes in the mains frequency */
                        /* Interpolate the zero crossing by successive approx. */
                        z = V_sample - phase->last_V_sample;
                        x = 0;
                        y = 0;
                        for (k = 0;  k < 8;  k++)
                        {
                            y <<= 1;
                            z >>= 1;
                            x += z;
                            if (x > V_sample)
                                x -= z;
                            else
                                y |= 1;
                        }
                        /* Now we need to allow for skipped samples, due to spike detection */
                        z = y;
                        while (phase->since_last > 1)
                        {
                            z += y;
                            phase->since_last--;
                        }
                        /* z is now the fraction of a sample interval between the zero
                           crossing and the current sample, in units of 1/256 of a sample */
                        /* A lightly damped filter should now be enough to remove noise and get a
                           stable value for the frequency */
                        phase->mains_period += ((int32_t) (phase->cycle_sample_count - z) << 12) - (phase->mains_period >> 4);
                        /* Start the next cycle with the residual fraction of a sample */
                        phase->cycle_sample_count = z;
                        
                        phase->status |= ZERO_CROSSING;
                        phase->zero_crossings++;
                        phase->NoZeroCrossing = 0;
                        phase->period_time = 0;                        
                    }
                    else
                    {
                        phase->cycle_sample_count = 0;
                        if (phase->period_time > (SAMPLES_PER_SECONDS/90)) { // check that mains has not collapsed
                          phase->period_time = 0;
                          phase->status |= ZERO_CROSSING;
                          phase->NoZeroCrossing = 0x80;
                        }                           
                    }
#endif
#if defined(POWER_FACTOR_SUPPORT)
                    /* Determine whether the current leads or lags, in a noise tolerant manner.
                       Testing 50 cycles means we will respond in about one second to a genuine
                       swap between lead and lag. Since that is also about the length of our
                       measurement blocks, this seems a sensible response time. */
                    if (I_live_sample < V_sample)
                    {
                        if (phase->current[0].leading > -50)
                            phase->current[0].leading--;
                    }
                    else
                    {
                        if (phase->current[0].leading < 50)
                            phase->current[0].leading++;
                    }

#endif
                    /* See if a sufficiently long measurement interval has been
                       recorded, and catch the start of the next cycle. We do not
                       really care how many cycles there are, as long as the block
                       is a reasonable length. Setting a minimum of 1 second is
                       better than counting cycles, as it is not affected by noise
                       spikes. Synchronising to a whole number of cycles reduces
                       block to block jitter, though it doesn't affect the long
                       term accuracy of the measurements. */
                    if (phase->sample_count >= (SAMPLES_PER_SECONDS/50) )
                    {

                        log_parameters();
                        /* The foreground may be conserving power (e.g. in limp mode), so we
                           need to kick it. */
                        _BIC_SR_IRQ(LPM0_bits);
                    }

                    //set leading edge cross zero event interrupt if respond IE is set

                }
                else if (phase->period_time > (SAMPLES_PER_SECONDS/90)) { // check that mains has not collapsed
                  phase->period_time = 0;
                  phase->status |= ZERO_CROSSING;
                  phase->NoZeroCrossing = 0x80;
                }                  
             
                /* Log the sign of the signal */
                phase->status |= V_POS;
            }
            phase->since_last = 0;
            phase->last_V_sample = V_sample;
        }// if (abs(V_sample - phase->last_V_sample) <= phase->since_last*MAX_PER_SAMPLE_VOLTAGE_SLEW)
        phase->since_last++;
        phase->period_time++;

        if (phase->sample_count >= SAMPLES_PER_SECONDS)
        {
            /* We don't seem to be detecting the end of a mains cycle, so force
               the end of processing block condition. */
            log_parameters();

            /* The foreground may be conserving power (e.g. in limp mode), so we
               need to kick it. */
            _BIC_SR_IRQ(LPM0_bits);
        }

        if (I_live_sample < 0)
        {
            /* Log the sign of the signal */
            phase->status &= ~I_POS;
        }
        else
        {
            phase->status |= I_POS;
        }


    }

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Generate energy pulse/////////////////
    /* We now play the last measurement interval's power level, evaluated
       in the foreground, through this measurement interval. In this way
       we can evenly pace the pulsing of the LED. The only error produced
       by this is the ambiguity in the number of samples per measurement.
       This should not exceed 1 or 2 in over 4000. */
    int32_t xx;
    if ((phase->status & CURRENT_FROM_NEUTRAL))
      xx = phase->active_power[1];
    else
      xx = phase->active_power[0];
    //accumulate all power, no matter positive or negtive, but here we change negtive power to positive to facillate pulse generation.
    //This will not affect the sign of power stored.
    if((gCsgconf&CSGCON_EMOD_MASK)==CSGCON_EMOD0){
      if(xx < 0)
        xx = -xx;
    }
    else{
      if((gCsgconf&CSGCON_EMOD_MASK)==CSGCON_EMOD1){//accumulate no negtive power
        if(xx < 0)
          xx = 0;
      }
    }


    if ((phase->energy.total_active_power_counter += xx) >= gCurrent_threshold)
    {
      phase->energy.total_active_power_counter -= gCurrent_threshold;


      if ((gPower_const <= 1000)&&(++phase->energy.extra_total_active_power_counter < 16)){
        ;//in extra power counter mode, and not accumulate enough power, skipped
      }
      else{
          phase->energy.extra_total_active_power_counter = 0;
          ++phase->energy.total_consumed_energy;
                /* Ideally we want to log the energy each kWh unit, but doing
                   it with a mask here is good enough and faster. */
          if ((phase->energy.total_consumed_energy & 0x3FF) == 0)
            phase->status |= ENERGY_LOGABLE;
                /* Pulse the LED. Long pulses may not be reliable, as at full
                   power we may be pulsing many times per second. People may
                   check the meter's calibration with an instrument that counts
                   the pulsing rate, so it is important the pulses are clear,
                   distinct, and exactly at the rate of one per
                   1/ENERGY_PULSES_PER_KW_HOUR kW/h. */
          if(gCsgconf&CSGCON_PPEN_MASK){
            set_total_energy_pulse_indicator();
            phase->energy.total_active_energy_pulse_remaining_time = ENERGY_PULSE_DURATION;
          }
      }
    }
    if(gCsgconf&CSGCON_PPEN_MASK){
        if (phase->energy.total_active_energy_pulse_remaining_time  &&  --phase->energy.total_active_energy_pulse_remaining_time == 0)
        {
            clr_total_energy_pulse_indicator();
        }
    }

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////Pick up possible left samples /////////////////
    /* There may be some current samples available, which we need to pick up */

    if ((SD24CCTL_LIVE & SD24IFG))
    {
        adc_buffer[1] = SD24MEM_LIVE;
        SD24CCTL_LIVE &= ~SD24IFG;
    }

#undef i
#undef use_stage
}
